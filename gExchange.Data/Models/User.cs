﻿using System;
using System.Collections.Generic;
using System.Text;
using gExchange.Data.Enums;

namespace gExchange.Data.Models
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public string Mail { get; set; }
        public bool IsDeleted { get; set; }
        public Enums.Type Type { get; set; }
        public string Password { get; set; }     
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime PenaltyExpirationDate { get; set; }

        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
        public List<Offer> Offers { get; set; }
        public List<Item> Items { get; set; }
        //sent Messages
        public List<Message> Messages { get; set; }

        //rec Messages
        public List<Message> RecMessages { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace gExchange.Data.Models
{
    public class AuthorizationToken
    {
        public int Id { get; set; }
        public string Value { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime Time { get; set; }
        public string IpAddress { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Models
{
    public class Message : IEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public int SenderId { get; set; }
        //sender User
        public User User { get; set; }

        public int RecipienId { get; set; }

        public User RecUser { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}

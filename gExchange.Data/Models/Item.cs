﻿using gExchange.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Models
{

    public class Item : IEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public ItemState ItemState { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<Image> Images { get; set; }

        public List<Offer> Offers { get; set; }

        public List<Comment> Comments { get; set; }

        public int CreatedBy { get; set; }

        public int UpdatedBy { get; set; }

        public int OwnerId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public User User { get; set; }
    }
}

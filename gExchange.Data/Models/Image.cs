﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Models
{
    public class Image : IEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Path { get; set; }

        public int? ItemId { get; set; }

        public byte[] File { get; set; }
        public bool IsDeleted { get; set; }
    }
}

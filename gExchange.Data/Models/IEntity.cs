﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Models
{
    public interface IEntity
    {
        int Id { get; set; }
        bool IsDeleted { get; set; }
    }
}

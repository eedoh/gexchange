﻿using gExchange.Data.Enums;
using System;

namespace gExchange.Data.Models
{
    public class Offer : IEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public int SenderId { get; set; }

        public User User { get; set; }

        public int ItemId { get; set; }

        public string Note { get; set; }

        public double Price { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public OfferState OfferState { get; set; }

    }
}

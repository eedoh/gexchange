﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Enums
{
    public enum OfferState
    {
        Pending,
        Accepted,
        Denied
    }
}

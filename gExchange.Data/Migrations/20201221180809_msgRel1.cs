﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gExchange.Data.Migrations
{
    public partial class msgRel1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Message_RecipienId",
                table: "Message",
                column: "RecipienId");

            migrationBuilder.AddForeignKey(
                name: "FK_Message_User_RecipienId",
                table: "Message",
                column: "RecipienId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message_User_RecipienId",
                table: "Message");

            migrationBuilder.DropIndex(
                name: "IX_Message_RecipienId",
                table: "Message");
        }
    }
}

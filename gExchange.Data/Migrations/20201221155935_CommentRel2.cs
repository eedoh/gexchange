﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gExchange.Data.Migrations
{
    public partial class CommentRel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ItemId1",
                table: "Comment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemId2",
                table: "Comment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Comment_ItemId1",
                table: "Comment",
                column: "ItemId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Item_ItemId1",
                table: "Comment",
                column: "ItemId1",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Item_ItemId1",
                table: "Comment");

            migrationBuilder.DropIndex(
                name: "IX_Comment_ItemId1",
                table: "Comment");

            migrationBuilder.DropColumn(
                name: "ItemId1",
                table: "Comment");

            migrationBuilder.DropColumn(
                name: "ItemId2",
                table: "Comment");
        }
    }
}

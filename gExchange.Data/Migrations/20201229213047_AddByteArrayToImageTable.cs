﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace gExchange.Data.Migrations
{
    public partial class AddByteArrayToImageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "File",
                table: "Image",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Image",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "File",
                table: "Image");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Image");
        }
    }
}

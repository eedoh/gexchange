﻿using gExchange.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace gExchange.Data
{
    public class ExchangeContext : DbContext
    {
        public ExchangeContext(DbContextOptions<ExchangeContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasOne(im => im.Image)
                .WithMany()
                .HasForeignKey(u => u.ImageId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Offers)
                .WithOne(of => of.User)
                .HasForeignKey(of => of.SenderId);

            modelBuilder.Entity<Item>()
                   .HasOne(p => p.User)
                   .WithMany(b => b.Items)
                   .HasForeignKey(p => p.OwnerId);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Messages)
                .WithOne(m => m.User)
                .HasForeignKey(m => m.SenderId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<User>()
                .HasMany(u => u.RecMessages)
                .WithOne(m => m.RecUser)
                .HasForeignKey(m => m.RecipienId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Item>()
                .HasMany(u => u.Comments)
                .WithOne()
                .HasForeignKey(c => c.ItemId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Item>()
                .HasMany(u => u.Offers)
                .WithOne()
                .HasForeignKey(c => c.ItemId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Item>()
               .HasMany(u => u.Images)
               .WithOne()
               .HasForeignKey(c => c.ItemId)
               .OnDelete(DeleteBehavior.Restrict);
        }

        public DbSet<Item> Item { set; get; }
        public DbSet<Image> Image { set; get; }
        public DbSet<Comment> Comment { set; get; }
        public DbSet<Message> Message { set; get; }
        public DbSet<Offer> Offer { set; get; }
        public DbSet<User> User { set; get; }

        public DbSet<AuthorizationToken> AuthorizationToken { set; get; }
    }
}

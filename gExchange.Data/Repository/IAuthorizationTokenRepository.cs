﻿using gExchange.Data.Models;

namespace gExchange.Data.Repository
{
    public interface IAuthorizationTokenRepository : IBaseRepository<AuthorizationToken>
    {
        void deleteAuthorizationToken(string token);
    }
}

﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using gExchange.Data.Models;

namespace gExchange.Data.Repository
{
    public class UserRepository : BaseRepository<User>,IUserRepository
    {   
        public UserRepository(ExchangeContext context):base(context)
        {     
        }

        public User getUserByIdAndPassword(string userName, string password)
        {
            return this._exchangeContext.User.SingleOrDefault(x => x.Mail == userName && x.Password == password);
        }

        public User getUserByToken(string token)
        {
            return this._exchangeContext.AuthorizationToken.Where(x => x.Value == token)
                .Select(s => s.User)
                .SingleOrDefault();
        }

        public User getByAndMessages(int id)
        {
            return this._exchangeContext.User.Include(x=>x.RecMessages).ThenInclude(y=>y.User).Include(y=>y.Messages).ThenInclude(y=>y.User).SingleOrDefault(x => x.Id == id);
        }
    }
}

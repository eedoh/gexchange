﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Repository
{
    public class OfferRepository : BaseRepository<Offer>, IOfferRepository
    {
        public OfferRepository(ExchangeContext context) : base(context)
        {
        }
    }
}

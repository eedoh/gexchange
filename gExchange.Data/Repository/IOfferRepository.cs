﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Repository
{
    public interface IOfferRepository : IBaseRepository<Offer>
    {
    }
}

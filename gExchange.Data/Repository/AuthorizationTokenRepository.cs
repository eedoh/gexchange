﻿using gExchange.Data.Models;
using System.Linq;

namespace gExchange.Data.Repository
{
    public class AuthorizationTokenRepository : BaseRepository<AuthorizationToken>, IAuthorizationTokenRepository
    {
        public AuthorizationTokenRepository(ExchangeContext context) : base(context)
        {
        }

        public void deleteAuthorizationToken(string token)
        {
            AuthorizationToken oldToken = this._exchangeContext.AuthorizationToken.FirstOrDefault(x => x.Value == token);
            if(oldToken != null)
            {
                this._exchangeContext.AuthorizationToken.Remove(oldToken);
                this._exchangeContext.SaveChanges();
            }

        }
    }
}

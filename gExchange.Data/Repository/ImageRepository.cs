﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gExchange.Data.Repository
{
    public class ImageRepository : BaseRepository<Image>, IImageRepository
    {
        public ImageRepository(ExchangeContext context) : base(context)
        {
        }

        public Image GetById(long id)
        {
            return _exchangeContext.Image.FirstOrDefault(i => i.Id == id);
        }

        public byte[] GetBinaryById(long id)
        {
            return GetById(id).File;
        }

        public List<Image> GetByName(string name)
        {
            return _exchangeContext.Image.Where(i => i.Title == name).ToList();
        }
    }
}

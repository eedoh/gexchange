﻿using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Data.Repository
{
    public interface IBaseRepository<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();

        Task<TEntity> AddAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);

        Task<TEntity> UpdateValuesAsync(TEntity entity, TEntity entry);

        TEntity GetById(int id);

        void Delete(TEntity entity);
    }
}

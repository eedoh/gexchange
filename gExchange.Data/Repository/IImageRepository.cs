﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Repository
{
    public interface IImageRepository : IBaseRepository<Image>
    {
        Image GetById(long id);

        byte[] GetBinaryById(long id);

        List<Image> GetByName(string name);
    }
}

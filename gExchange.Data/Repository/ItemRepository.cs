﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace gExchange.Data.Repository
{
    public class ItemRepository : BaseRepository<Item>, IItemRepository
    {
        public ItemRepository(ExchangeContext context) : base(context)
        {
        }

        public Item getByIdRelPopulated(int id)
        {
            return this._exchangeContext.Item
                .Include(y=>y.User)
                .Include(y => y.Comments).ThenInclude(x=>x.User)
                .Include(y => y.Offers).ThenInclude(x=>x.User)
                .FirstOrDefault(x => x.Id == id);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Data.Repository
{
    /// <summary>
    ///  The "where" clause in a generic definition specifies for generic TEntity type constraints which must be of class type.
    ///  Also "new()" constraint makes it possible to create an instance of type paramater by using new operator.
    ///  SAMPLE CODE [T GetItem{ return new T();}]
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, new()
    {
        protected readonly ExchangeContext _exchangeContext;

        public BaseRepository(ExchangeContext exchangeContext)
        {
            _exchangeContext = exchangeContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return _exchangeContext.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await _exchangeContext.AddAsync(entity);
                await _exchangeContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                _exchangeContext.Update(entity);
                await _exchangeContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateValuesAsync(TEntity entity, TEntity entry)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                _exchangeContext.Entry(entry).CurrentValues.SetValues(entity);
                await _exchangeContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }
        public virtual TEntity GetById(int id)
        {
            return _exchangeContext.Set<TEntity>().Find(id);
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                _exchangeContext.Entry(entity).State = EntityState.Deleted;
                _exchangeContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be deleted: {ex.Message}");
            }
          
        }
    }
}

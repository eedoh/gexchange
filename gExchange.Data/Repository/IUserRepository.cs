﻿using gExchange.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace gExchange.Data.Repository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User getUserByIdAndPassword(string userName, string password);

        User getUserByToken(string token);

        User getByAndMessages(int id);
    }
}

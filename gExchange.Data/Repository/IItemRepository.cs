﻿using gExchange.Data.Models;

namespace gExchange.Data.Repository
{
    public interface IItemRepository : IBaseRepository<Item>
    {
        Item getByIdRelPopulated(int id);
    }
   
}

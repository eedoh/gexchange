﻿using gExchange.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace gExchange.Data.Repository
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        public MessageRepository(ExchangeContext context) : base(context)
        {
        }
    }
}

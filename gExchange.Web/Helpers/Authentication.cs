﻿using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;

namespace gExchange.Web.Helpers
{
    public static class Authentication
    {
        private const string LoggedUser = "logged_user";

        public async static Task SetLoggedUser(this HttpContext context, UserVM user, bool snimiUCookie = false)
        {
            IAuthorizationTokenService _atService = context.RequestServices.GetService<IAuthorizationTokenService>();

            string oldToken = context.Request.GetCookieJson<string>(LoggedUser);
            if (oldToken != null)
            {
                _atService.delete(oldToken);
            }

            if (user != null)
            {

                string token = Guid.NewGuid().ToString();
                await _atService.add(new AuthorizationTokenVM
                {
                    Value = token,
                    UserId = user.Id,
                });
                context.Response.SetCookieJson(LoggedUser, token);
            }
        }

        public static string GetCurrentToken(this HttpContext context)
        {
            return context.Request.GetCookieJson<string>(LoggedUser);
        }

        public static UserVM GetLoggedUser(this HttpContext context)
        {
            IUserService _userService = context.RequestServices.GetService<IUserService>();

            string token = context.Request.GetCookieJson<string>(LoggedUser);
            if (token == null)
                return null;

            return _userService.getUserByToken(token);
        }
    }
}

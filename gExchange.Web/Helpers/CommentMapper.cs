﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{

    public class CommentMapper
    {
        UserMapper _userMapper = null;

        public CommentMapper()
        {
            _userMapper = new UserMapper();
        }

        public CommentVM CommentToVM(Comment comment)
        {
            return new CommentVM
            {
                Id = comment.Id,
                IsDeleted = comment.IsDeleted,
                ItemId = comment.ItemId,
                UserId = comment.UserId,
                Text = comment.Text,
                CreatedAt = comment.CreatedAt,
                UpdatedAt = comment.UpdatedAt,
                User = comment.User != null ? _userMapper.UserToVm(comment.User):null,
            };
        }

        public Comment VmToComment(CommentVM comment)
        {
            return new Comment
            {
                Id = comment.Id,
                IsDeleted = comment.IsDeleted,
                ItemId = comment.ItemId,
                UserId = comment.UserId,
                Text = comment.Text,
                CreatedAt = comment.CreatedAt,
                UpdatedAt = comment.UpdatedAt,
                User = comment.User != null ? _userMapper.VmToUser(comment.User) : null,
            };
        }
    }

   
}

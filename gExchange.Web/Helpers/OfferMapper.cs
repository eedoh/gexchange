﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class OfferMapper
    {
        UserMapper _userMapper = null;
        public OfferMapper()
        {
            _userMapper = new UserMapper();
        }

        public OfferVM OfferToVM(Offer offer)
        {
            return new OfferVM
            {
                Id = offer.Id,
                IsDeleted = offer.IsDeleted,
                SenderId = offer.SenderId,
                ItemId = offer.ItemId,
                Note = offer.Note,
                Price = offer.Price,
                CreatedAt = offer.CreatedAt,
                UpdatedAt = offer.UpdatedAt,
                OfferState = offer.OfferState,
                User = offer.User != null ? _userMapper.UserToVm(offer.User): null,
            };
        }

        public Offer VmToOffer(OfferVM offer)
        {
            return new Offer
            {
                Id = offer.Id,
                IsDeleted = offer.IsDeleted,
                SenderId = offer.SenderId,
                ItemId = offer.ItemId,
                Note = offer.Note,
                Price = offer.Price,
                CreatedAt = offer.CreatedAt,
                UpdatedAt = offer.UpdatedAt,
                OfferState = offer.OfferState,
                User = offer.User != null ? _userMapper.VmToUser(offer.User) : null,
            };
        }
    }
}

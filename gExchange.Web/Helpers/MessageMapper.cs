﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class MessageMapper
    {
        UserMapper _userMapper = null;
        public MessageMapper(){
            _userMapper = new UserMapper();
        }

        public MessageVM MessageToVM(Message message)
        {
            return new MessageVM
            {
                Id = message.Id,
                IsDeleted = message.IsDeleted,
                SenderId = message.SenderId,
                
                RecipienId = message.RecipienId,
                Title = message.Title,
                Text = message.Text,
                CreatedAt = message.CreatedAt,
                User = message.User != null ? _userMapper.UserToVm(message.User) :null,

            };
        }

        public Message VmToMessage(MessageVM message)
        {
            return new Message
            {
                Id = message.Id,
                IsDeleted = message.IsDeleted,
                SenderId = message.SenderId,

                RecipienId = message.RecipienId,
                Title = message.Title,
                Text = message.Text,
                CreatedAt = message.CreatedAt,
                User = message.User != null ? _userMapper.VmToUser(message.User) : null,

            };
        }
    }
}

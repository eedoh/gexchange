﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class UserMapper
    {
        public UserMapper()
        {
        }

        public UserVM UserToVm(User user)
        {
            return new UserVM
            {             
                Id = user.Id,
                Mail = user.Mail,
                IsDeleted = user.IsDeleted,
                Type = user.Type,
                Password  = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CreatedAt = user.CreatedAt,
                UpdatedAt = user.UpdatedAt,
                PenaltyExpirationDate = user.PenaltyExpirationDate,

                //Messages = 
                //Offers = 
                //Items = 
                //Image = 
            };
        }

        public User VmToUser(UserVM user)
        {
            return new User
            {
                Id = user.Id,
                Mail = user.Mail,
                IsDeleted = user.IsDeleted,
                Type = user.Type,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CreatedAt = user.CreatedAt,
                UpdatedAt = user.UpdatedAt,
                PenaltyExpirationDate = user.PenaltyExpirationDate,

                //Messages = user.Messages != null && user.Messages.Count > 0 ?
                //user.Messages.Select(x => _messageMapper.VmToMessage(x)).ToList() : new List<Message>()

                //Offers = 
                //Items =  
                //Image = 
            };
        }
    }
}

﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class AuthorizationTokenMapper
    {
        UserMapper _userMapper = null;
        public AuthorizationTokenMapper()
        {
            _userMapper = new UserMapper();
        }

        public AuthorizationTokenVM AtToVM(AuthorizationToken at)
        {
            return new AuthorizationTokenVM
            {
                Id = at.Id,
                Value = at.Value,
                UserId = at.UserId,
                Time = at.Time,
                IpAddress = at.IpAddress,
                User = at.User !=null ? _userMapper.UserToVm(at.User):null
            };
        }

        public AuthorizationToken VMToAt(AuthorizationTokenVM at)
        {
            return new AuthorizationToken
            {
                Id = at.Id,
                Value = at.Value,
                UserId = at.UserId,
                Time = at.Time,
                IpAddress = at.IpAddress,
                User = at.User !=null ? _userMapper.VmToUser(at.User):null
            };
        }
    }
}

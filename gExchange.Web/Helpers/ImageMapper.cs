﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class ImageMapper
    {
        public ImageMapper()
        {
        }

        public ImageVM ImageToVm(Image image)
        {
            return new ImageVM
            {
                Id = image.Id,
                IsDeleted = image.IsDeleted,
                Binary = image.File,
                FileName = image.Title,
                Path = image.Path
            };
        }

        public Image VmToImage(ImageVM image)
        {
            return new Image
            {
                Id = image.Id,
                IsDeleted = image.IsDeleted,
                File = image.Binary,
                Path = image.Path,
                Title = image.FileName

                // vidio sam da prosljedjujete createdAT i modifiedAt ovdje
                // Ja to ne bih radio
                // Nego bih prosirio base entity da to radi automatski
                // Nije tesko
            };
        }

        public byte[] StreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}

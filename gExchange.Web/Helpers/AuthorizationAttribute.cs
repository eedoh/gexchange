﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace gExchange.Web.Helpers
{
    public class AuthorizationAttribute : TypeFilterAttribute
    {
        public AuthorizationAttribute(bool admin, bool user)
            : base(typeof(MyAuthorizeImpl))
        {
            Arguments = new object[] { admin, user };
        }
    }


    public class MyAuthorizeImpl : IAsyncActionFilter
    {
        public MyAuthorizeImpl(bool admin, bool user)
        {
            _admin = admin;
            _user = user;
        }
        private readonly bool _admin;
        private readonly bool _user;
        public async Task OnActionExecutionAsync(ActionExecutingContext filterContext, ActionExecutionDelegate next)
        {
            UserVM k = filterContext.HttpContext.GetLoggedUser();

            if (k == null)
            {
                if (filterContext.Controller is Controller controller)
                {
                    controller.TempData["error_message"] = "You aren't logged";
                }

                filterContext.Result = new RedirectToActionResult("Index", "Auth", new { @area = "" });
                return;
            }

            //Preuzimamo DbContext preko app services
            //IUserService _userService = filterContext.HttpContext.RequestServices.GetService<IUserService>();
            //UserVM user = _userService.getById(k.Id);
            

            //useri mogu pristupiti 
            if (_user && k.Type.Equals(Data.Enums.Type.User))
            {
                await next(); //ok - ima pravo pristupa
                return;
            }

            //admini mogu pristupiti 
            if (_admin && k.Type.Equals(Data.Enums.Type.Admin))
            {
                await next();//ok - ima pravo pristupa
                return;
            }

            if (filterContext.Controller is Controller c1)
            {
                c1.ViewData["error_message"] = "You don't have access rights!";
            }
            filterContext.Result = new RedirectToActionResult("Index", "Home", new { @area = "" });
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // throw new NotImplementedException();
        }
    }
}

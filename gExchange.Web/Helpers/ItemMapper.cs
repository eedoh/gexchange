﻿using gExchange.Data.Models;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Helpers
{
    public class ItemMapper
    {
        UserMapper _userMapper;
        OfferMapper _offerMapper;
        CommentMapper _commentMapper;

        public ItemMapper()
        {
            _userMapper = new UserMapper();
            _offerMapper = new OfferMapper();
            _commentMapper = new CommentMapper();
        }

        public ItemVM ItemToVm(Item item)
        {
            
            return new ItemVM
            {
                Id = item.Id,
                IsDeleted = item.IsDeleted,
                ItemState = item.ItemState,
                Title = item.Title,
                Description = item.Description,
                CreatedBy = item.CreatedBy,
                UpdatedBy = item.UpdatedBy,
                OwnerId = item.OwnerId,
                CreatedAt = item.CreatedAt,
                UpdatedAt = item.UpdatedAt,

                User = item.User != null ? _userMapper.UserToVm(item.User) : null,
                //TODO: 
                //Images = 
                Offers = item.Offers != null && item.Offers.Count > 0 ?
                item.Offers.Select(x => _offerMapper.OfferToVM(x)).ToList() : new List<OfferVM>(),

                Comments = item.Comments != null && item.Comments.Count > 0 ?
                item.Comments.Select(x => _commentMapper.CommentToVM(x)).ToList() : new List<CommentVM>()

            };
        }

        public Item VmToItem(ItemVM item)
        {
            
            return new Item
            {
                Id = item.Id,
                IsDeleted = item.IsDeleted,
                ItemState = item.ItemState,
                Title = item.Title,
                Description = item.Description,
                CreatedBy = item.CreatedBy,
                UpdatedBy = item.UpdatedBy,
                OwnerId = item.OwnerId,
                CreatedAt = item.CreatedAt,
                UpdatedAt = item.UpdatedAt,

                User = item.User != null ? _userMapper.VmToUser(item.User) : null,
                //Images = 
                Offers = item.Offers != null && item.Offers.Count > 0 ?
                item.Offers.Select(x => _offerMapper.VmToOffer(x)).ToList() : new List<Offer>(),
                
                Comments = item.Comments != null && item.Comments.Count > 0 ?
                item.Comments.Select(x => _commentMapper.VmToComment(x)).ToList() : new List<Comment>()
            };
        }
    }
}

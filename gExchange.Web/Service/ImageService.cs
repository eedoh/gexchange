﻿using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class ImageService : IImageService
    {
        private readonly IImageRepository _imageRepository;

        //vidio sam da ste mapper ubacivali kao klasu pa sam i ja to uradio
        //ja bih radije stavio interface, moci cete raditi DI i bice code testabilniji.
        private ImageMapper _imageMapper = null;

        public ImageService (IImageRepository imageRepository)
        {
            _imageRepository = imageRepository;
            _imageMapper = new ImageMapper();
        }

        public async Task<ImageVM> Save(IFormFile image)
        {
            // create the VM
            ImageVM imageVM = new ImageVM();

            if (image != null && image.Length > 0)
            {
                imageVM.FileName = Path.GetFileName(image.FileName);
                imageVM.Path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\uploads", imageVM.FileName);

                using (var fileStream = new FileStream(imageVM.Path, FileMode.Create))
                {
                    // Save image to the FS
                    await image.CopyToAsync(fileStream);
                }

                using (var memoryStream = new MemoryStream())
                {
                    image.CopyTo(memoryStream);
                    var binaryData = memoryStream.ToArray();
                    // Add it to the VM too
                    imageVM.Binary = binaryData;
                }

                // Now save the binary to the DB
                await _imageRepository.AddAsync(_imageMapper.VmToImage(imageVM));

                // Return ImageVM
                return imageVM;
            }

            // Handlajte ovdje kako vama odgovara
            throw new Exception("Image can not be null or empty!!!");
        }

    }
}

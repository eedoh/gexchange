﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        CommentMapper _commentMapper = null;
        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
            _commentMapper = new CommentMapper();
        }

        public async Task<CommentVM> add(CommentVM comment, int userId)
        {
            Comment commentEntity = _commentMapper.VmToComment(comment);
            commentEntity.UpdatedAt = DateTime.Now;
            commentEntity.UserId = userId;
            if (commentEntity.Id <= 0)
            {
             
                commentEntity.CreatedAt = DateTime.Now;
                commentEntity = await _commentRepository.AddAsync(commentEntity);
            }
            else
            {
                Comment prevItem = _commentRepository.GetById(commentEntity.Id);
                if (userId == prevItem.UserId)
                {
                    commentEntity.CreatedAt = prevItem.CreatedAt;

                    commentEntity = await _commentRepository.UpdateAsync(commentEntity);
                }
                else
                {
                    throw new Exception("Unauthorized Edit");
                }

            }

            return _commentMapper.CommentToVM(commentEntity);
        }
    }
}

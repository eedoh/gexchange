﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IAuthorizationTokenService
    {
        Task<AuthorizationTokenVM> add(AuthorizationTokenVM item);
        void delete(string token);
    }
}

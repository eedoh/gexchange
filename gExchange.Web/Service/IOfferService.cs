﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IOfferService
    {
        Task<OfferVM> add(OfferVM offer, int userId);
    }
}

﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IMessageService
    {
        Task<MessageVM> add(MessageVM message, int userId);
    }
}

﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class OfferService : IOfferService
    {
        private readonly IOfferRepository _offerRepository;
        OfferMapper _offerMapper = null;
        public OfferService(IOfferRepository offerRepository)
        {
            _offerRepository = offerRepository;
            _offerMapper = new OfferMapper();
        }

        public async Task<OfferVM> add(OfferVM offer, int userId)
        {
            Offer offerEntity = _offerMapper.VmToOffer(offer);

            offerEntity.SenderId = userId;
            offerEntity.OfferState = Data.Enums.OfferState.Pending;
            if (offerEntity.Id <= 0)
            {

                offerEntity.CreatedAt = DateTime.Now;
                offerEntity = await _offerRepository.AddAsync(offerEntity);
            }
            else
            {
                throw new Exception("Unauthorized Edit");

            }

            return _offerMapper.OfferToVM(offerEntity);
        }
    }
}

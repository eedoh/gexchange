﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface ICommentService
    {
        Task<CommentVM> add(CommentVM comment, int userId);
    }
}

﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class UserService: IUserService
    {
        private readonly IUserRepository _userRepository;
        UserMapper _userMapper = null;
        MessageMapper _messageMapper;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _userMapper = new UserMapper();
            _messageMapper = new MessageMapper();
        }

        public RowsVM<UserVM> getUsers()
        {
            IQueryable<User> users = _userRepository.GetAll();
            return new RowsVM<UserVM>
            {
                rows = users.Select(x => _userMapper.UserToVm(x)).ToList()
            };
        }

        public async Task<UserVM> addUser(UserVM user)
        {
            User newUser = _userMapper.VmToUser(user);
            newUser.UpdatedAt = DateTime.Now;
            if (user.Id <= 0)
            {
                newUser.CreatedAt = DateTime.Now;
                newUser = await _userRepository.AddAsync(newUser);
            }
            else
            {
                User entry = _userRepository.GetById(newUser.Id);
                newUser = await _userRepository.UpdateValuesAsync(newUser, entry);
            }
            
           return _userMapper.UserToVm(newUser);
        }

        public UserVM getById(int id)
        {
            User user = _userRepository.GetById(id);
            if(user!=null)
             return _userMapper.UserToVm(user);

            return null;
        }

        public void deleteUser(int id)
        {
            User user = _userRepository.GetById(id);
            if(user!=null)
                _userRepository.Delete(user);
        }

        public UserVM getUserByIdAndPassword(string userName, string password)
        {
            User user = _userRepository.getUserByIdAndPassword(userName, password);
            if(user != null)
            {
                return _userMapper.UserToVm(user);
            }
            return null;
        }

        public UserVM getByAndMessages(int id)
        {
            User user = _userRepository.getByAndMessages(id);
            if (user != null)
            {
                List<MessageVM> msgs = user.Messages != null && user.Messages.Count > 0 ?
                user.Messages.Select(x => _messageMapper.MessageToVM(x)).ToList() : new List<MessageVM>();

                UserVM uservm = _userMapper.UserToVm(user);
                uservm.Messages = msgs;
                return uservm;
            }
            return null;
        }

        public UserVM getUserByToken(string token)
        {
            User user = _userRepository.getUserByToken(token);
            if (user != null)
            {
                return _userMapper.UserToVm(user);
            }
            return null;
        }
    }
}

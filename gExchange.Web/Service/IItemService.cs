﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IItemService
    {
       RowsVM<ItemVM> getAll();
       Task<ItemVM> add(ItemVM item, int userId);
       ItemVM getById(int id);
       void delete(int id);
    }
}

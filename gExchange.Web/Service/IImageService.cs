﻿using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IImageService
    {
        Task<ImageVM> Save(IFormFile image);
    }
}

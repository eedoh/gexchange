﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public interface IUserService
    {
       RowsVM<UserVM> getUsers();
       Task<UserVM> addUser(UserVM user);
       UserVM getById(int id);
       UserVM getByAndMessages(int id);
       void deleteUser(int id);
       UserVM getUserByIdAndPassword(string userName, string password);
       UserVM getUserByToken(string token);
    }
}

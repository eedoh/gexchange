﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;
        MessageMapper _messageMapper = null;
        public MessageService(IMessageRepository messsageRepository)
        {
            _messageRepository = messsageRepository;
            _messageMapper = new MessageMapper();
        }

        public async Task<MessageVM> add(MessageVM message, int userId)
        {
            Message messageEntity = _messageMapper.VmToMessage(message);
            
            messageEntity.SenderId = userId;
            if (messageEntity.Id <= 0)
            {

                messageEntity.CreatedAt = DateTime.Now;
                messageEntity = await _messageRepository.AddAsync(messageEntity);
            }
            else
            {
                throw new Exception("Unauthorized Edit");

            }

            return _messageMapper.MessageToVM(messageEntity);
        }
    }
}

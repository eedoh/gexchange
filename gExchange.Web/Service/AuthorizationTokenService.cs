﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class AuthorizationTokenService : IAuthorizationTokenService
    {

        private readonly IAuthorizationTokenRepository _authorizationTokenRepository;
        AuthorizationTokenMapper _atMapper = null;
        public AuthorizationTokenService(IAuthorizationTokenRepository authorizationTokenRepository)
        {
            _authorizationTokenRepository = authorizationTokenRepository;
            _atMapper = new AuthorizationTokenMapper();
        }

        public async Task<AuthorizationTokenVM> add(AuthorizationTokenVM item)
        {
            AuthorizationToken newItem = _atMapper.VMToAt(item);
            newItem.Time = DateTime.Now;
            newItem = await _authorizationTokenRepository.AddAsync(newItem);

            return _atMapper.AtToVM(newItem);
        }

        public void delete(string token)
        {
            _authorizationTokenRepository.deleteAuthorizationToken(token);
        }
    }
}

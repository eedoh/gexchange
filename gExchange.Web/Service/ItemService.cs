﻿using gExchange.Data.Models;
using gExchange.Data.Repository;
using gExchange.Web.Helpers;
using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.Service
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository _itemRepository;
        ItemMapper _itemMapper = null;
        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
            _itemMapper = new ItemMapper();
        }

        public RowsVM<ItemVM> getAll()
        {
            IQueryable<Item> items = _itemRepository.GetAll();
            return new RowsVM<ItemVM>
            {
                rows = items.Select(x => _itemMapper.ItemToVm(x)).ToList()
            };
        }

        public async Task<ItemVM> add(ItemVM item, int userId)
        {
            Item newItem = _itemMapper.VmToItem(item);
            newItem.UpdatedAt = DateTime.Now;
            newItem.UpdatedBy = userId;
            if (item.Id <= 0)
            {
                newItem.CreatedBy = userId;
                newItem.CreatedAt = DateTime.Now;
                newItem.OwnerId = userId;
                newItem = await _itemRepository.AddAsync(newItem);
            }
            else
            {
                Item prevItem =  _itemRepository.GetById(item.Id);
                if( userId == prevItem.OwnerId)
                {
                    newItem.CreatedBy = prevItem.CreatedBy;
                    newItem.CreatedAt = prevItem.CreatedAt;

                    newItem = await _itemRepository.UpdateAsync(newItem);
                }
                else
                {
                    throw new Exception("Unauthorized Edit");
                }
               
            }
            
           return _itemMapper.ItemToVm(newItem);
        }

        public ItemVM getById(int id)
        {
            Item item =  _itemRepository.getByIdRelPopulated(id);
            return _itemMapper.ItemToVm(item);
        }

        public void delete(int id)
        {
            Item item = _itemRepository.GetById(id);
            _itemRepository.Delete(item);
        }
    }
}

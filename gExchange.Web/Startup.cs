using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


using Microsoft.EntityFrameworkCore;
using gExchange.Data.Repository;
using gExchange.Web.Service;

namespace gExchange.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ExchangeContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("local")));
            services.AddControllersWithViews();
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();

            /*Transient objects are always different; a new instance is provided to every controller and every service.

            Scoped objects are the same within a request, but different across different requests.

            Singleton objects are the same for every object and every request.*/

            services.AddTransient(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IImageRepository, ImageRepository>();
            services.AddTransient<IItemRepository, ItemRepository>();
            services.AddTransient<IAuthorizationTokenRepository, AuthorizationTokenRepository>();

            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IOfferRepository, OfferRepository>();
            services.AddTransient<ICommentRepository, CommentRepository>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IImageService, ImageService>();
            services.AddTransient<IItemService, ItemService>();
            services.AddTransient<IAuthorizationTokenService, AuthorizationTokenService>();

            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IOfferService, OfferService>();
            services.AddTransient<ICommentService, CommentService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

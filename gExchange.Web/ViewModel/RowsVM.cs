﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class RowsVM<T>
    {
        public List<T> rows { get; set; }
    }
}

﻿using gExchange.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class ItemVM
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public ItemState ItemState { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Title { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 2)]
        public string Description { get; set; }

        public List<ImageVM> Images { get; set; }

        public List<OfferVM> Offers { get; set; }

        public List<CommentVM> Comments { get; set; }

        public int CreatedBy { get; set; }

        public int UpdatedBy { get; set; }

        public int OwnerId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public UserVM User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class AuthorizationTokenVM
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int UserId { get; set; }
        public UserVM User { get; set; }
        public DateTime Time { get; set; }
        public string IpAddress { get; set; }
    }
}

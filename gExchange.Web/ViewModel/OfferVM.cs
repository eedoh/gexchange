﻿using gExchange.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class OfferVM
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public int SenderId { get; set; }

        public UserVM User { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int ItemId { get; set; }

        [StringLength(200, MinimumLength = 2)]
        public string Note { get; set; }

        [Required]
        [Range(typeof(double), "1", "99999", ErrorMessage = "Price must be a number between {1} and {2}.")]
        public double Price { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public OfferState OfferState { get; set; }
    }
}

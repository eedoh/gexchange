﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class ImageVM
    {
        //ne kontam sto ce vam id i isdeleted u VMu, ali evo da se ne pravim pametan i ja sam ih ubacio
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public byte[] Binary { get; set; }
    }
}

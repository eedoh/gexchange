﻿using gExchange.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace gExchange.Web.ViewModel
{
    public class UserVM
    {
        public int Id { get; set; }

        [Required, EmailAddress]
        public string Mail { get; set; }
        public bool IsDeleted { get; set; }
        public Data.Enums.Type Type { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime PenaltyExpirationDate { get; set; }
        public string FileName { get; set; }

        public List<OfferVM> Offers { get; set; }

        public List<ItemVM> Items { get; set; }

        public List<MessageVM> Messages { get; set; }

        public List<MessageVM> RecMessages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class SignupVM
    {
        [Required]
        [StringLength(100, ErrorMessage = "Password must contain min 4 characters.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required, EmailAddress]
        [StringLength(30, ErrorMessage = "Must be valid email address")]
        [DataType(DataType.EmailAddress)]
        public string Mail { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First name must contain min 2 characters.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50,ErrorMessage = "Last name must contain min 2 characters ", MinimumLength = 2)]
        public string LastName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class LoginVM
    {
        [Required]
        [StringLength(100, ErrorMessage = "Username must contain min 3 characters.", MinimumLength = 3)]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Password must contain min 4 characters.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberPassword { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace gExchange.Web.ViewModel
{
    public class CommentVM
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int ItemId { get; set; }

        public int UserId { get; set; }

        public UserVM User { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 2)]
        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}

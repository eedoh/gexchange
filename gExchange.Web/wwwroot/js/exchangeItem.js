﻿$(document).ready(function () {
    // izvršava nakon što glavni html dokument bude generisan
    $("#items-container").load("/Item/GetItems");
    $("#show-items").click(function () {
        $("#save-item").addClass("hidden")
        $("#items-container").load("/Item/GetItems");
     
    });

    $("#add-item").click(function () {
        $("#items-container").load("/Item/GetItem");
        $("#save-item").removeClass("hidden")
    });

    $("#save-item").click(function () {

        $.validator.unobtrusive.parse($("#item-form"));
        var valid = $("#item-form").valid();
        
        if (valid) {

            $.ajax({
                url: '/Item/CreateItem',
                type: "POST",
                data: $("#item-form").serialize(),
                success: function (responce) {
                    console.log(responce.Message);
                    $("#items-container").load("/Item/GetItems");
                    $("#save-item").addClass("hidden")
                   
                },
                error: function (xhr, textStatus) {
                    alert(xhr.status + " " + xhr.statusText);
                }
            });
        }    
    });
});

$(document).ajaxComplete(function () {
    // izvršava nakon bilo kojeg ajax poziva
   
});

﻿$(document).ready(function () {
  
    $("#add-message-container").load("/Message/GetAdd");
    $("#add-offer-container").load("/Offer/GetAdd");
    $("#add-comment-container").load("/Comment/GetAdd");

    $(".details-container-sec-nav span").click(function () {
        var secAttr = $(this).attr("con-attr");
        $(".details-container-sec-nav span").each(function (i) {
            $(this).removeClass("active")
        });
        $(this).addClass("active")

        $(".details-comm-container").each(function (i) {
            $(this).addClass("hidden")
        });
        $("#"+secAttr).closest(".details-comm-container").removeClass("hidden")
    });


    //offer po_add_item_id =  $("#po_add_item_id").val($("#dItemId").val())
    //message pm_add_recipient_id -> user id =  $("#pm_add_recipient_id").val($("#dUserId").val())
    //comment pc_add_item_id = $("#pc_add_item_id").val($("#dItemId").val())
    $("#save-comment").click(function () {
        var formName = "add-comment-form"
        var containerName = "add-comment-container"
        $.validator.unobtrusive.parse($("#" + formName));
        var valid = $("#" + formName).valid();
        $("#pc_add_item_id").val($("#dItemId").val())

        if (valid) {
            var serData = $("#" + formName).serialize()
           
            $.ajax({
                url: '/Comment/CreateComment',
                type: "POST",
                data: serData,
                success: function (responce) {
                    console.log(responce.Message);
                    $("#" + containerName).load("/Comment/GetAdd");

                },
                error: function (xhr, textStatus) {
                    alert(xhr.status + " " + xhr.statusText);
                }
            });
        }
    });

    $("#save-offer").click(function () {
        var formName = "add-offer-form"
        var containerName = "add-offer-container"
        $.validator.unobtrusive.parse($("#" + formName));
        var valid = $("#" + formName).valid();

        if (valid) {
            $("#po_add_item_id").val($("#dItemId").val())
            $.ajax({
                url: '/Offer/CreateOffer',
                type: "POST",
                data: $("#" + formName).serialize(),
                success: function (responce) {
                    console.log(responce.Message);
                    $("#" + containerName).load("/Offer/GetAdd");

                },
                error: function (xhr, textStatus) {
                    alert(xhr.status + " " + xhr.statusText);
                }
            });
        }
    });

    $("#save-message").click(function () {
        var formName = "add-message-form"
        var containerName = "add-message-container"
        $.validator.unobtrusive.parse($("#" + formName));
        var valid = $("#" + formName).valid();

        if (valid) {
            $("#pm_add_recipient_id").val($("#dUserId").val())
            $.ajax({
                url: '/Message/CreateMessage',
                type: "POST",
                data: $("#" + formName).serialize(),
                success: function (responce) {
                    console.log(responce.Message);
                    $("#" + containerName).load("/Message/GetAdd");

                },
                error: function (xhr, textStatus) {
                    alert(xhr.status + " " + xhr.statusText);
                }
            });
        }
    });
});

$(document).ajaxComplete(function () {
    // izvršava nakon bilo kojeg ajax poziva

});

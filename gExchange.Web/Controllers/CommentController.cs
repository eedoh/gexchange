﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using gExchange.Web.Helpers;

namespace gExchange.Web.Controllers
{
    public class CommentController : Controller
    {
        readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        public IActionResult GetAdd()
        {
            return PartialView("_add", new CommentVM());
        }

        public async Task<JsonResult> CreateComment(CommentVM comment)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    throw new Exception("Please correct the following errors: " + Environment.NewLine + messages);
                }
                UserVM user = HttpContext.GetLoggedUser();
                await _commentService.add(comment, user.Id);

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Web.Helpers;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Type = gExchange.Data.Enums.Type;

namespace gExchange.Web.Controllers
{
    public class AuthController : Controller
    {
        readonly IUserService _userService;
        readonly IAuthorizationTokenService _atService;
        public AuthController(IUserService userService, IAuthorizationTokenService atService)
        {
            _userService = userService;
            _atService = atService;
        }

        public IActionResult Index()
        {
            return View(new LoginVM()
            {
                RememberPassword = true,
            });
        }

        public IActionResult Register()
        {
            return View();
        }

        public async Task<IActionResult> Login(LoginVM input)
        {
            if (ModelState.IsValid)
            {
                UserVM user = _userService.getUserByIdAndPassword(input.Username, input.Password);

                if (user == null)
                {
                    TempData["error_message"] = "Wrong username or password";
                    return View("Index", input);
                }

                await HttpContext.SetLoggedUser(user, input.RememberPassword);

                return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Login", "Auth");
        }

        public async Task<IActionResult> Signup(SignupVM input)
        {
            if(ModelState.IsValid)
            {
                var user = new UserVM
                {
                    FirstName = input.FirstName,
                    LastName = input.LastName,
                    Password = input.Password,
                    Mail = input.Mail,
                    Type = Type.User
                };
                
                // Check if user is already registered
                var existingUser = _userService.getUserByIdAndPassword(user.Mail, user.Password);

                if (existingUser != null)
                {
                    TempData["error_message"] = "User with provided email already exist !";
                    return View("Register", input);
                }
                
                var createdUser = await _userService.addUser(user);

                // Try to login automatically with new credentials
                if (createdUser != null)
                {
                    await HttpContext.SetLoggedUser(createdUser, true);
                }

                return RedirectToAction("Index", "Home");
            } 
            else
                return RedirectToAction("Register", "Auth");
            
        }

        public IActionResult Logout()
        {
            //Clear logged_user cookie from browser
            //or delete AuthorizationToken Record
            _atService.delete(HttpContext.GetCurrentToken());
            return RedirectToAction("Index");
        }
    }
}

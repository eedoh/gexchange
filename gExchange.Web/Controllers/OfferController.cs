﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using gExchange.Web.Helpers;

namespace gExchange.Web.Controllers
{
    public class OfferController : Controller
    {
        readonly IOfferService _offerService;

        public OfferController(IOfferService offerService)
        {
            _offerService = offerService;
        }

        public IActionResult GetAdd()
        {
            return PartialView("_add", new OfferVM());
        }

        public async Task<JsonResult> CreateOffer(OfferVM offer)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    string offers = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    throw new Exception("Please correct the following errors: " + Environment.NewLine + offers);
                }
                UserVM user = HttpContext.GetLoggedUser();
                await _offerService.add(offer, user.Id);

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Offer = ex.Message });
            }
        }
    }
}

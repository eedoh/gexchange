﻿using System.IO;
using System.Threading.Tasks;
using gExchange.Web.Helpers;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gExchange.Web.Controllers
{
    [AuthorizationAttribute(admin: true, user:true)]
    public class UserController : Controller
    {
        readonly IUserService _userService;
        readonly IImageService _imageService;

        public UserController(IUserService userService, IImageService imageService)
        {
            _userService = userService;
            _imageService = imageService;
        }
        
        public ActionResult Index()
        {
            RowsVM<UserVM> userRows = _userService.getUsers();
            return View("Index", userRows);
        }

        public ViewResult Add()
        {
            var model = new UserVM();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Add(UserVM user)
        {
            // Dodavanje fajla
            if (ModelState.IsValid)
            {
                IFormFile file = (HttpContext.Request.Form.Files.Count > 0) ? HttpContext.Request.Form.Files[0] : null;
                await _imageService.Save(file);
            }

            // Stari code
            await _userService.addUser(user);
            return RedirectToAction("Index");
        }

        public ViewResult Edit(int id)
        {
            var model = _userService.getById(id);
            return View("Add", model);
        }

        public ViewResult Delete(int id)
        {
            var model = _userService.getById(id);
            return View("Delete", model);
        }

        [HttpPost]
        public IActionResult DeleteItem(int id)
        {
            _userService.deleteUser(id);
            return RedirectToAction("Index");
        }

        public IActionResult Profile()
        {
            UserVM user = HttpContext.GetLoggedUser();
            UserVM model = _userService.getByAndMessages(user.Id);
            return View("Profile", model);
        }
    }
}

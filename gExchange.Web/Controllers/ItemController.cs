﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gExchange.Web.Helpers;
using gExchange.Web.Service;
using gExchange.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace gExchange.Web.Controllers
{
    [AuthorizationAttribute(admin: true, user: true)]
    public class ItemController : Controller
    {
        readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Details(int id)
        {
            ItemVM item = _itemService.getById(id);
            return View("Details", item);
        }

        public IActionResult GetItems()
        {
            RowsVM<ItemVM> items = _itemService.getAll();
            return PartialView("_listPartial", items);
        }

        public IActionResult GetItem()
        {
            return PartialView("_addPartial", new ItemVM());
        }

        [HttpPost]
        public async Task<JsonResult> CreateItem(ItemVM item)
        {
            
            try
            {
                if (!ModelState.IsValid)
                {
                    string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    throw new Exception("Please correct the following errors: " + Environment.NewLine + messages);
                }
                UserVM user = HttpContext.GetLoggedUser();
                await _itemService.add(item, user.Id);

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
